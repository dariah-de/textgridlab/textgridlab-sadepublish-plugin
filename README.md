# SADE publish plugin for TextGridLab

This is a TextGridLab plugin to publish data from the TextGridLab to [SADE](https://gitlab.gwdg.de/SADE/SADE) instances.

To use this plugin activate it from the marketplace inside TextGridLab or add https://dariah-de.pages.gwdg.de/textgridlab/textgridlab-sadepublish-plugin/ as an update site to TextGridLab.

## Release / Development

This project uses the [Git-Flow Maven plugin](https://github.com/aleksandr-m/gitflow-maven-plugin)

For a list of goals enter

        mvn gitflow:help

start a new feature

        mvn gitflow:feature-start

finish feature

        mvn gitflow:feature-finish

finish and squash commits

        mvn gitflow:feature-finish -DfeatureSquash=true


