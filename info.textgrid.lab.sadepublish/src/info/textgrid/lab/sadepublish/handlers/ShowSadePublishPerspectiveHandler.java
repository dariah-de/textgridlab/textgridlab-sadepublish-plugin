/*
 * #%L
 * info.textgrid.lab.sadepublish
 * %%
 * Copyright (C) 2011 TextGrid Consortium (http://www.textgrid.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.lab.sadepublish.handlers;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.sadepublish.Activator;
import info.textgrid.lab.sadepublish.views.SadePublishView;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.handlers.HandlerUtil;

public class ShowSadePublishPerspectiveHandler  extends AbstractHandler implements IHandler{

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {

			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			wb.getActiveWorkbenchWindow().getActivePage().setPerspective(
					wb.getPerspectiveRegistry().findPerspectiveWithId(
							"info.textgrid.lab.sadepublish.perspectives.SadePublishPerspective"));
			wb.getActiveWorkbenchWindow().getActivePage().resetPerspective();

			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.sadepublish.perspectives.SadePublishPerspective",
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
			wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());

			// test if command was called from navigator
			if(event.getCommand().getId().equals("info.textgrid.lab.sadepublish.publishSelected")) {

				ISelection selection = HandlerUtil.getCurrentSelection(event);
				if (selection instanceof IStructuredSelection) {

					for (Object obj : ((IStructuredSelection) selection).toList()){

						if (obj instanceof IAdaptable) {
							TextGridObject tgo = (TextGridObject) ((IAdaptable) obj)
									.getAdapter(TextGridObject.class);

							String title = tgo.getTitle();
							String uri = tgo.getURI().toString();
							String contentType = tgo.getMetadata().getGeneric().getProvided().getFormat();

							((SadePublishView)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
								.findView("info.textgrid.lab.sadepublish.views.SadePublishView")).addTextGridObject(uri, title, contentType);

						}
					}
				}
			}

		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    "Could not open Sade Publisher Perspective!", e);
			Activator.getDefault().getLog().log(status);
		} catch (CoreException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    "Error !", e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}

}
