/*
 * #%L
 * info.textgrid.lab.sadepublish
 * %%
 * Copyright (C) 2011 TextGrid Consortium (http://www.textgrid.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.lab.sadepublish.views;


import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.sadepublish.Activator;
import info.textgrid.lab.sadepublish.preferences.PluginPreferencePage;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;


public class SadePublishView extends ViewPart {

	private Browser spBrowser;
	private boolean browserReady = false;
	Queue<String> jsExecutions = new LinkedList<String>(); // queue to for JavaScript commands to execute which where issued before browserReady

	@Override
	public void createPartControl(Composite parent) {

    	try {
    		spBrowser = new Browser(parent, SWT.NONE);
    	} catch (SWTError e) {
			System.out.println("Could not instantiate Browserview " + e.getMessage());
			return;
		}

    	String url = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.sadeUrl_id, "", null);

    	if(url.equals("")) {
    		spBrowser.setText("No publish destination, please set one with Window->Preferences->Sade Publisher");
    		return;
    	}

    	spBrowser.setUrl(url);
		spBrowser.addProgressListener(new ProgressListener() {
			@Override
			public void changed(ProgressEvent arg0) {}

			@Override
			public void completed(ProgressEvent arg0) {

				String sid = RBACSession.getInstance().getSID(false);
				spBrowser.execute("setSid('"+sid+"')");

				String user = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.sadeUser_id, "", null);
				String pw = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.sadePW_id, "", null);
                spBrowser.execute("setAuth('"+user+"','"+pw+"')");
				browserReady = true;

				// execute JavaScript commands which where collected before browser was ready
				Iterator<String> it = jsExecutions.iterator();
				while(it.hasNext()) {
					String command = it.next();
					spBrowser.execute(command);
					it.remove();
				}
			}
		});


	}

	public void addTextGridObject(String uri, String title, String contentType) {

		String command = "addTGObject('"+uri+"', '"+title + " ("+uri+")"+"', '"+contentType+"')";
		if(browserReady) {
			spBrowser.execute(command);
		} else {
			jsExecutions.add(command);
		}
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		//viewer.getControl().setFocus();
	}
}
