package info.textgrid.lab.sadepublish.tester;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.CoreException;

public class IsPublishableObject extends PropertyTester {

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {

		if (receiver != null && receiver instanceof TGObjectReference) {
			return isOK(AdapterUtils.getAdapter(receiver, TextGridObject.class));
		}

		return true;
	}

	private boolean isOK(TextGridObject tgo) {
		String contentTypeId = "";
//		reloadMetadata(tgo);
		try {
			contentTypeId = tgo.getContentTypeID();
			if (   contentTypeId.contains("tg.aggregation")
					|| contentTypeId.contains("xml")
					|| contentTypeId.contains("image/")
					|| contentTypeId.contains("text/linkeditorlinkedfile")
					|| contentTypeId.contains("application/x-shockwave-flash")){
				return true;
			}
		} catch (CoreException e) {
			info.textgrid.lab.sadepublish.Activator
					.handleWarning(e,
							"Couldn't get the content type id of the selected textgrid object");
		}

		return false;
	}
}
